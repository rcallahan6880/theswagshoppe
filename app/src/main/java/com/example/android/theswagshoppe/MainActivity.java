package com.example.android.theswagshoppe;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    public static final String TEXT_EMAIL = "text/email";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
//   Create email on click
    public void emailButton(View view) {
        TextView emailButton = findViewById(R.id.emailButton);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(TEXT_EMAIL);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { getString(R.string.emailAddressTo)});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.emailSubject));
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.emailMessage));

        startActivity(Intent.createChooser(intent, getString(R.string.emailIntent)));

    }
}
